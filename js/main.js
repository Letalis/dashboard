
if(typeof(_LANGUAGE)=='undefined') var _LANGUAGE='nl_NL';
if(typeof(_USE_FAVORITES)=='undefined') var _USE_FAVORITES=false;
if(typeof(_USE_AUTO_POSITIONING)=='undefined') var _USE_AUTO_POSITIONING=false;
if(typeof(_HIDE_SECONDS_IN_CLOCK)=='undefined') var _HIDE_SECONDS_IN_CLOCK=false;
if(typeof(_HIDE_MEDIAPLAYER_WHEN_OFF)=='undefined') var _HIDE_MEDIAPLAYER_WHEN_OFF=false;
if(typeof(_USE_FAHRENHEIT)=='undefined') var _USE_FAHRENHEIT=false;
if(typeof(_BACKGROUND_IMAGE)=='undefined') var _BACKGROUND_IMAGE='bg2.jpg';
if(typeof(_NEWS_RSSFEED)=='undefined') var _NEWS_RSSFEED			= 'http://www.nu.nl/rss/algemeen';
if(typeof(_STANDBY_AFTER_MINUTES)=='undefined') var _STANDBY_AFTER_MINUTES = 10;
if(typeof(_WEATHER_CITYNAME)=='undefined') var _WEATHER_CITYNAME = '';
if(typeof(_SCROLL_NEWS_AFTER)=='undefined') var _SCROLL_NEWS_AFTER = 6500;
if(typeof(_STREAMPLAYER_TRACKS)=='undefined') var _STREAMPLAYER_TRACKS = {"track":1,"name":"Music FM","file":"http://stream.musicfm.hu:8000/musicfm.mp3"};

var _TEMP_SYMBOL = '°C';
if(_USE_FAHRENHEIT) _TEMP_SYMBOL = '°F';

$.ajax({url: 'vendor/jquery/touchpunch.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/jquery/jquery-ui.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/bootstrap/js/bootstrap.min.js', async: false,dataType: "script"});
$.ajax({url: 'js/functions.js', async: false,dataType: "script"});
		
$.ajax({url: 'CONFIG.js', async: false,dataType: "script"});
$.ajax({url: 'lang/'+_LANGUAGE+'.js', async: false,dataType: "script"});

$.ajax({url: 'vendor/raphael/raphael-min.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/morrisjs/morris.min.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/moment.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/moment-with-locales.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/nzbget/nzbget.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/jquery.newsTicker.min.js', async: false,dataType: "script"});
$.ajax({url: 'vendor/jquery.newsTicker.min.js', async: false,dataType: "script"});

$.ajax({url: 'js/switches.js', async: false,dataType: "script"});
$.ajax({url: 'js/blocks.js', async: false,dataType: "script"});
$.ajax({url: 'js/weather.js', async: false,dataType: "script"});
$.ajax({url: 'js/graphs.js', async: false,dataType: "script"});
$.ajax({url: 'js/json_vb.js', async: false,dataType: "script"});
$.ajax({url: 'js/graph_vb.js', async: false,dataType: "script"});
$.ajax({url: 'js/traffic.js', async: false,dataType: "script"});
$.ajax({url: 'js/news.js', async: false,dataType: "script"});
$.ajax({url: 'js/ns.js', async: false,dataType: "script"});

var standby=true;
var standbyActive=false;
var standbyTime=0;
var req;
var slide;
var sliding = false;
var defaultcolumns=false;
var allblocks={}
var alldevices={}

if(typeof(columns)=='undefined') defaultcolumns = true;
var _GRAPHS_LOADED = new Object();
_GRAPHREFRESH = 5;

if(typeof(blocks)=='undefined') var blocks = {}
if(typeof(columns_standby)=='undefined') var columns_standby = {}

if(typeof(screens)=='undefined'){
	var screens = {}
	screens[1] = {}
	screens[1]['background'] = _BACKGROUND_IMAGE;
	screens[1]['columns'] = []
	if(defaultcolumns===false){
		for(c in columns){
			screens[1]['columns'].push(c);
		}
	}
}
$(document).ready(function(){	
	$('body').attr('unselectable','on')
     .css({'-moz-user-select':'-moz-none',
           '-moz-user-select':'none',
           '-o-user-select':'none',
           '-khtml-user-select':'none',
           '-webkit-user-select':'none',
           '-ms-user-select':'none',
           'user-select':'none'
     }).bind('selectstart', function(){ return false; });
	
	buildScreens();
	
	setTimeout(function(){
		setInterval(function(){ 
			if(_HIDE_SECONDS_IN_CLOCK==true) $('.clock').html(moment().locale(_LANGUAGE.substr(0,2)).format('HH:mm'));
			else $('.clock').html(moment().locale(_LANGUAGE.substr(0,2)).format('HH:mm:ss'));
			$('.date').html(moment().locale(_LANGUAGE.substr(0,2)).format('D MMMM YYYY'));
			$('.weekday').html(moment().locale(_LANGUAGE.substr(0,2)).format('dddd'));
		},1000);
		
		getDevices(); 		
	},750);
	
	setClassByTime();
	setInterval(function(){ 
		setClassByTime();
	},(60000));
}); 

function buildStandby(){
	
	if($('.screenstandby').length==0){
		var screenhtml = '<div class="screen screenstandby fullslider slidestandby"><div class="row"></div></div>';
		$('div.screen').hide();
		$('div.contents').prepend(screenhtml);	

		for(c in columns_standby){
			$('div.screenstandby .row').append('<div class="col-xs-'+columns_standby[c]['width']+' colstandby'+c+'"></div>');
			for(b in columns_standby[c]['blocks']){
				$('.block_'+columns_standby[c]['blocks'][b]).first().clone().appendTo('.colstandby'+c);
			}
		}
	}
}
function buildScreens(){
	var num=1;
	for(s in screens){
		var screenhtml = '<div class="screen screen'+s+' fullslider slide'+s+'"';
		if(typeof(screens[s]['background'])!=='undefined') screenhtml+='style="background-image:url(\'img/'+screens[s]['background']+'\');"';
		screenhtml+='><div class="row"></div></div>';
		$('div.contents').append(screenhtml);	
		if(objectlength(screens)>1){
			var check = '';
			if(num==1){
				check='checked';
			}
			$('.slider-pagination').before('<input type="radio" name="slider" class="slide-radio'+num+'" '+check+' id="slider_'+num+'">');
			$('.slider-pagination').append('<label for="slider_'+num+'" class="paginate page'+num+'"></label>');
		}
		
		
		if(defaultcolumns===false){
			for(cs in screens[s]['columns']){
				c = screens[s]['columns'][cs];
				if(typeof(columns[c])!=='undefined'){
					$('div.screen'+s+' .row').append('<div class="col-xs-'+columns[c]['width']+' col'+c+'"></div>');
					for(b in columns[c]['blocks']){
						
						var width=12;
						if(typeof(blocks[columns[c]['blocks'][b]])!=='undefined' && typeof(blocks[columns[c]['blocks'][b]]['width'])!=='undefined') width = blocks[columns[c]['blocks'][b]]['width'];

						var blocktype='';
						if(typeof(blocks[columns[c]['blocks'][b]])!=='undefined' && typeof(blocks[columns[c]['blocks'][b]]['type'])!=='undefined') blocktype = blocks[columns[c]['blocks'][b]]['type'];

						if(blocktype=='blocktitle'){
							$('div.screen'+s+' .row .col'+c).append('<div class="col-xs-12 mh titlegroups transbg"><h3>'+blocks[columns[c]['blocks'][b]]['title']+'</h3></div>');
						}
						else if(columns[c]['blocks'][b]=='weather'){
							$('div.screen'+s+' .row .col'+c).append('<div class="block_'+columns[c]['blocks'][b]+' containsweatherfull"></div>');
							if(_APIKEY_WUNDERGROUND!=="" && _WEATHER_CITY!=="") loadWeatherFull(_WEATHER_CITY,_WEATHER_COUNTRY,$('.weatherfull'));
						}
						else if(columns[c]['blocks'][b]=='currentweather' || columns[c]['blocks'][b]=='currentweather_big'){
							var cl = '';
							if(columns[c]['blocks'][b]=='currentweather_big') $('div.screen'+s+' .row .col'+c).append('<div class="mh transbg big block_'+columns[c]['blocks'][b]+' col-xs-'+width+' containsweather"><div class="col-xs-1"><div class="weather" id="weather"></div></div><div class="col-xs-11"><span class="title weatherdegrees" id="weatherdegrees"></span> <span class="weatherloc" id="weatherloc"></span></div></div>');
							else $('div.screen'+s+' .row .col'+c).append('<div class="mh transbg block_'+columns[c]['blocks'][b]+' col-xs-'+width+' containsweather"><div class="col-xs-4"><div class="weather" id="weather"></div></div><div class="col-xs-8"><strong class="title weatherdegrees" id="weatherdegrees"></strong><br /><span class="weatherloc" id="weatherloc"></span></div></div>');
							if(_APIKEY_WUNDERGROUND!=="" && _WEATHER_CITY!=="") loadWeather(_WEATHER_CITY,_WEATHER_COUNTRY);
						}
						else if(columns[c]['blocks'][b]=='train'){
							$('div.screen'+s+' .row .col'+c).append('<div class="train"></div>');
							getTrainInfo();
						}
						else if(columns[c]['blocks'][b]=='traffic'){
							$('div.screen'+s+' .row .col'+c).append('<div class="traffic"></div>');
							getTraffic();
						}
						else if(columns[c]['blocks'][b]=='news'){
							$('div.screen'+s+' .row .col'+c).append('<div class="news"></div>');
							getNews('news',_NEWS_RSSFEED);
						}
						else if(typeof(columns[c]['blocks'][b])=='string' && columns[c]['blocks'][b].substring(0,5)=='news_'){
							$('div.screen'+s+' .row .col'+c).append('<div class="'+columns[c]['blocks'][b]+'"></div>');
							getNews(columns[c]['blocks'][b],blocks[columns[c]['blocks'][b]]['feed']);
						}
						else if(columns[c]['blocks'][b]=='clock'){
							$('div.screen'+s+' .row .col'+c).append('<div class="transbg block_'+columns[c]['blocks'][b]+' col-xs-'+width+' text-center"><h1 class="clock"></h1><h4 class="weekday"></h4><h4 class="date"></h4></div>');
						}
						else if(columns[c]['blocks'][b]=='sunrise'){
							$('div.screen'+s+' .row .col'+c).append('<div class="block_'+columns[c]['blocks'][b]+' col-xs-'+width+' transbg text-center sunriseholder"><em class="wi wi-sunrise"></em><span class="sunrise"></span><em class="wi wi-sunset"></em><span class="sunset"></span></div>');
						}
						else if(typeof(columns[c]['blocks'][b])=='object' && typeof(columns[c]['blocks'][b]['isimage'])!=='undefined'){
							var random = getRandomInt(1,100000);
							$('div.screen'+s+' .row .col'+c).append(loadImage(random,columns[c]['blocks'][b]));
						}
						else if(columns[c]['blocks'][b]=='horizon'){
							var html ='<div class="containshorizon">';
									html+='<div class="col-xs-4 transbg hover text-center" onclick="ziggoRemote(\'E0x07\')">';
										html+='<em class="fa fa-chevron-left fa-small"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" onclick="ziggoRemote(\'E4x00\')">';
										html+='<em class="fa fa-pause fa-small"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" onclick="ziggoRemote(\'E0x06\')">';
										html+='<em class="fa fa-chevron-right fa-small"></em>';
									html+='</div>';
								html+='</div>';
							$('div.screen'+s+' .row .col'+c).append(html);
						}
						else if(columns[c]['blocks'][b]=='streamplayer'){
							var html ='<div class="transbg containsstreamplayer">';
									html+='<div class="col-xs-12 transbg smalltitle" id="npTitle"><h3></h3></div>';
									html+='<audio id="audio1" preload="none"></audio>';
									html+='<div class="col-xs-4 transbg hover text-center" id="btnPrev">';
										html+='<em class="fa fa-chevron-left fa-small"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" id="playStream">';
										html+='<em class="fa fa-play fa-small" id="stateicon"></em>';
									html+='</div>';
									html+='<div class="col-xs-4 transbg hover text-center" id="btnNext">';
										html+='<em class="fa fa-chevron-right fa-small"></em>';
									html+='</div>';
								html+='</div>';
							$('div.screen'+s+' .row .col'+c).append(html);
							

							var supportsAudio = !!document.createElement('audio').canPlayType;
  							if(supportsAudio) {
  							  var index = 0,
  							  playing = false;
  							  tracks = _STREAMPLAYER_TRACKS,
  							  trackCount = tracks.length,
  							  npTitle = $('#npTitle h3'),
  							  audio = $('#audio1').bind('play', function() {
  							  	//$('#npTitle').fadeIn('slow');
  							    $('#stateicon').removeClass('fa fa-play');
  							    $('#stateicon').addClass('fa fa-pause');
  							    playing = true;
  							  }).bind('pause', function() {
  							  	
  							    $('#stateicon').removeClass('fa fa-pause');
  							    $('#stateicon').addClass('fa fa-play');
  							    playing = false;
  							  }).get(0),
  							  btnPrev = $('#btnPrev').click(function() {
  							    if((index - 1) > -1) {
  							      index--;
  							      loadTrack(index);
  							      if(playing) {
  							        audio.play();
  							      }
  							    } else {
  							      index = 0
  							      loadTrack(trackCount-1);
  							      if(playing) {
  							        audio.play();
  							      }
  							    }
  							  }),
  							  btnNext = $('#btnNext').click(function() {
  							  	
  							  	if((index + 1) < trackCount) {
  							      index++;
  							      loadTrack(index);
  							      
  							      if(playing) {
  							      	//console.log("hit the play button"+playing);
  							        audio.play();
  							      }

  							    } else {
  							      index = 0;
  							      loadTrack(index);
  							      if(playing) {
  							        audio.play();
  							      }
  							    }
  							   // $('#npTitle').fadeIn('slow');
  							    //audio.pause();
  							  }),
  							  loadTrack = function(id) {
  							    npTitle.text(tracks[id].name);
  							    index = id;
  							    audio.src = tracks[id].file;
  							  };
  							  loadTrack(index);
  							}
  							
							$( "#playStream" ).click(function() {
							  var myAudio = $("#audio1").get(0);
							  if (myAudio.paused) {
   							   $('#stateicon').removeClass('fa fa-play');
   							   $('#stateicon').addClass('fa fa-pause');
   							   myAudio.play();
   							 } else {
   							   //$('#npTitle').fadeOut('slow');	
   							   $('#stateicon').removeClass('fa fa-pause');
   							   $('#stateicon').addClass('fa fa-play');
   							   myAudio.pause();
   							}
							});
												
						}
						else if(typeof(columns[c]['blocks'][b])=='object'){
							var random = getRandomInt(1,100000);
							if(typeof(columns[c]['blocks'][b]['frameurl'])!=='undefined') $('div.screen'+s+' .row .col'+c).append(loadFrame(random,columns[c]['blocks'][b]));
							else $('div.screen'+s+' .row .col'+c).append(loadButton(b,columns[c]['blocks'][b]));
						}
						else {
							$('div.screen'+s+' .row .col'+c).append('<div class="mh transbg block_'+columns[c]['blocks'][b]+'"></div>');
						}
					}
				}
			}
		}
		else {
			$('body .row').append('<div class="col-xs-5 col1"><div class="auto_switches"></div><div class="auto_dimmers"></div></div>');
			$('body .row').append('<div class="col-xs-5 col2"><div class="block_weather containsweatherfull"></div><div class="auto_media"></div><div class="auto_states"></div></div>');
			$('body .row').append('<div class="col-xs-2 col3"><div class="auto_clock"></div><div class="auto_sunrise"></div><div class="auto_buttons"></div></div>');

			$('.col2').prepend('<div class="mh transbg big block_currentweather_big col-xs-12 containsweather"><div class="col-xs-1"><div class="weather" id="weather"></div></div><div class="col-xs-11"><span class="title weatherdegrees" id="weatherdegrees"></span> <span class="weatherloc" id="weatherloc"></span></div></div>');
			if(_APIKEY_WUNDERGROUND!=="" && _WEATHER_CITY!==""){
				loadWeatherFull(_WEATHER_CITY,_WEATHER_COUNTRY,$('#weatherfull'));
				loadWeather(_WEATHER_CITY,_WEATHER_COUNTRY);
			}

			$('.col3 .auto_clock').html('<div class="transbg block_clock col-xs-12 text-center"><h1 id="clock" class="clock"></h1><h4 id="weekday" class="weekday"></h4><h4 id="date" class="date"></h4></div>');
			$('.col3 .auto_sunrise').html('<div class="block_sunrise col-xs-12 transbg text-center sunriseholder"><em class="wi wi-sunrise"></em><span id="sunrise" class="sunrise"></span><em class="wi wi-sunset"></em><span id="sunset" class="sunset"></span></div>');
			for(b in buttons){
				if(buttons[b].isimage) $('.col3 .auto_buttons').append(loadImage(b,buttons[b]));
				else $('.col3 .auto_buttons').append(loadButton(b,buttons[b]));
			}
		}
		num++;
	}
}

function setClassByTime(){
	if($('input[name="slider"]:checked').length>0){
		
		var d = new Date();
		var n = d.getHours();

		if (n >= 20 || n <=5){
			newClass = 'night';
		}
		else if (n >= 6 && n <= 10) {
			newClass = 'morning';
		}
		else if (n >= 11 && n <= 15) {
			newClass = 'noon';
		}
		else if (n >= 16 && n <=19) {
			newClass = 'afternoon';
		}
		
		for(s in screens){
			if(typeof(screens[s]['background_'+newClass])!=='undefined'){
				$('.screen.screen'+s).css('background-image','url(\'img/'+screens[s]['background_'+newClass]+'\')');
			}
		}
		
		$('body').removeClass('morning noon afternoon night').addClass(newClass);
	}
}

//STANDBY FUNCTION
if(parseFloat(_STANDBY_AFTER_MINUTES)>0){
   setInterval(function(){
      standbyTime+=1000;
   },1000);

   setInterval(function(){
      if(standbyActive!=true){
         if(standbyTime>=((_STANDBY_AFTER_MINUTES*1000)*60)){
            $('body').addClass('standby');
			if(objectlength(columns_standby)>0) buildStandby();
            if(typeof(_STANDBY_CALL_URL)!=='undefined' && _STANDBY_CALL_URL!==''){
               $.get(_STANDBY_CALL_URL);
               standbyActive=true;
				
            }
         }
      }
   },5000);

   /*
   $('body').bind('mousemove', function(e){
      standbyTime=0;
      disableStandby();
   });
   */
	
   $('body').bind('touchstart click', function(e){
      standbyTime=0;
      disableStandby();
   });

   function disableStandby(){
      if(standbyActive==true){
         standbyTime=0;
         if(typeof(_END_STANDBY_CALL_URL)!=='undefined' && _END_STANDBY_CALL_URL!==''){
               $.get(_END_STANDBY_CALL_URL);
         }
      }
	  
	  if(objectlength(columns_standby)>0){
		$('div.screen').show();
	  	$('.screenstandby').remove();
	  }
      $('body').removeClass('standby');
      standbyActive=false;
      
   }
}
//END OF STANDBY FUNCTION

function loadButton(b,button){
	var random = getRandomInt(1,100000);
	if($('#button_'+b).length==0){
		var html = '<div class="modal fade" id="button_'+b+'_'+random+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
		  html+='<div class="modal-dialog">';
			html+='<div class="modal-content">';
			  html+='<div class="modal-header">';
				html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			  html+='</div>';
			  html+='<div class="modal-body">';
				  html+='<iframe data-src="'+button.url+'" width="100%" height="570" frameborder="0" allowtransparency="true"></iframe> '; 
			  html+='</div>';
			html+='</div>';
		  html+='</div>';
		html+='</div>';
		$('body').append(html);
	}
	
	var width = 12;
	if(typeof(button.width)!=='undefined') width=button.width;
	var html='<div class="col-xs-'+width+' hover transbg" data-toggle="modal" data-target="#button_'+b+'_'+random+'" onclick="setSrc(this);">';
		html+='<div class="col-xs-4 col-icon">';
			if(typeof(button.image)!=='undefined') html+='<img class="buttonimg" src="'+button.image+'" />';
			else html+='<em class="fa '+button.icon+' fa-small"></em>';
		html+='</div>';
		html+='<div class="col-xs-8 col-data">';
			html+='<strong class="title">'+button.title+'</strong><br>';
			html+='<span class="state"></span>';
		html+='</div>';
	html+='</div>';
	return html;
}

function loadFrame(f,frame){
	
	var width = 12;
	if(typeof(frame.width)!=='undefined') width=frame.width;
	var html='<div class="col-xs-'+width+' hover transbg imgblock imgblock'+f+'" style="height:'+frame.height+'px;padding:0px !important;">';
		html+='<div class="col-xs-12 col-data" style="padding:0px !important;">';
			html+='<iframe src="'+frame.frameurl+'" style="width:100%;border:0px;height:'+(frame.height-14)+'px;"></iframe>';
		html+='</div>';
	html+='</div>';
	
	var refreshtime = 60000;
	if(typeof(frame.refreshiframe)!=='undefined') refreshtime = frame.refreshiframe;
	setInterval(function(){ 
		reloadFrame(f,frame);
	},refreshtime);
	
	return html;
}

function reloadFrame(i,frame){
	var sep='?';
	
	if(typeof(frame.frameurl)!=='undefined'){
		var img = frame.frameurl;
		if (img.indexOf("?") != -1) var sep='&';

		if (img.indexOf("?") != -1){
			var newimg = img.split(sep+'t=');
			img = newimg;
		}
		img+=sep+'t='+(new Date()).getTime();
	}
	
	$('.imgblock'+i).find('iframe').attr('src',img);
}

function loadImage(i,image){

	if(typeof(image.image)!=='undefined'){
		var img = image.image;
	}
	
	if($('.imgblockopens'+i).length==0 && typeof(image.url)!=='undefined'){
		var html = '<div class="modal fade imgblockopens'+i+'" id="'+i+'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">';
		  html+='<div class="modal-dialog">';
			html+='<div class="modal-content">';
			  html+='<div class="modal-header">';
				html+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			  html+='</div>';
			  html+='<div class="modal-body">';
				  html+='<iframe data-src="'+image.url+'" width="100%" height="570" frameborder="0" allowtransparency="true"></iframe> '; 
			  html+='</div>';
			html+='</div>';
		  html+='</div>';
		html+='</div>';
		$('body').append(html);
	}

	var width = 12;
	if(typeof(image.width)!=='undefined') width=image.width;
	var html='';
	
	if(typeof(image.url)!=='undefined') html+='<div class="col-xs-'+width+' hover transbg imgblock imgblock'+i+'" data-toggle="modal" data-target="#'+i+'" onclick="setSrc(this);">';
	else html+='<div class="col-xs-'+width+' transbg imgblock imgblock'+i+'">';
	html+='<div class="col-xs-12">';

		html+='<img src="'+img+'" style="max-width:100%;" />';

	html+='</div>';
	html+='</div>';
	
	var refreshtime = 60000;
	if(typeof(image.refresh)!=='undefined') refreshtime = image.refresh;
	if(typeof(image.refreshimage)!=='undefined') refreshtime = image.refreshimage;
	setInterval(function(){ 
		reloadImage(i,image,true);
	},refreshtime);
	
	var refreshtime = 60000;
	if(typeof(image.refreshiframe)!=='undefined') refreshtime = image.refreshiframe;
	setInterval(function(){ 
		reloadIframe(i,image,true);
	},refreshtime);
	
	return html;
}

function reloadImage(i,image){
	var sep='?';
	
	if(typeof(image.image)!=='undefined'){
		var img = image.image;
		if (img.indexOf("?") != -1) var sep='&';

		if (img.indexOf("?") != -1){
			var newimg = img.split(sep+'t=');
			img = newimg;
		}
		img+=sep+'t='+(new Date()).getTime();
	}
	
	$('.imgblock'+i).find('img').attr('src',img);
}

function reloadIframe(i,image){
	var sep='?';
	
	if(typeof(image.url)!=='undefined'){
		var url = image.url;
		if (url.indexOf("?") != -1) var sep='&';

		if (url.indexOf("?") != -1){
			var newurl = url.split(sep+'t=');
			url = newurl;
		}
		url+=sep+'t='+(new Date()).getTime();
	}
	
	if(typeof($('.imgblockopens'+i+' iframe').attr('src')!=='undefined')){
	   
		$('.imgblockopens'+i+' iframe').attr('src',url);
	}
}

function getDevices(){
	if(!sliding){
		if(typeof(req)!=='undefined') req.abort();
		req = $.getJSONP({
			url: _HOST_DOMOTICZ+'/json.htm?type=devices&filter=all&used=true&order=Name&jsoncallback=?',
			type: 'GET',async: true,contentType: "application/json",dataType: 'jsonp',
			error: function( jqXHR, textStatus ) {
				alert("Domoticz error!\nPlease, double check the path in _HOST_DOMOTICZ-variable!");
			},
			success: function(data) {
				if(typeof(_DEBUG)!=='undefined' && _DEBUG) data = $.parseJSON(jsonexample);
				if(!sliding){
					$('.solar').remove();
				
					if($('.sunrise').length>0) $('.sunrise').html(data.Sunrise);
					if($('.sunset').length>0) $('.sunset').html(data.Sunset);
										
					for(r in data.result){
						
						var device = data.result[r];
						var idx = device['idx'];
						if(typeof(blocks)!=='undefined' && typeof(blocks[idx])!=='undefined' && typeof(blocks[idx]['title'])!=='undefined'){
							device['Name'] = blocks[idx]['title'];
						}
						
						if(device['Type']=='Group' || device['Type']=='Scene') var idx = 's'+device['idx'];
						alldevices[idx] = device;
						
						if(
							(
								_USE_AUTO_POSITIONING==true && 
								(
									(_USE_FAVORITES==true && device['Favorite']==1) || 
									_USE_FAVORITES===false
								)
							) ||
							$('.block_'+idx).length>0 ||
							$('.block_'+idx+'_1').length>0 ||
							$('.block_'+idx+'_2').length>0 ||
							$('.block_'+idx+'_3').length>0 ||
							$('.block_graph_'+idx).length>0
						){
							var width=4;
							if(device['SwitchType']=='Media Player') width=12;
							if(device['SwitchType']=='Dimmer') width=12;
							if(typeof(blocks)!=='undefined' && typeof(blocks[idx])!=='undefined' && typeof(blocks[idx]['width'])!=='undefined') width=blocks[idx]['width'];
							
							if($('.block_'+idx).length<=0){
								if(
									device['Type']=='Thermostat' || 
									device['Type']=='Temp + Humidity' || 
									device['Type']=='Temp + Humidity + Baro' || 
									device['Type']=='Usage' || 
									device['Type']=='Temp' || 
									device['Type']=='Humidity' || 
									device['Type']=='General' || 
									device['Type']=='Wind' || 
									device['Type']=='Rain' || 
									device['Type']=='RFXMeter' || 
									device['Type']=='P1 Smart Meter' || 
									device['Type']=='P1 Smart Meter USB' || 
									device['Type']=='Group' || 
									device['Type']=='Scene' || 
									device['SwitchType']=='Motion Sensor' || 
									device['SwitchType']=='Smoke Detector' || 
									device['SwitchType']=='Contact'
								){
									$('.col2 .auto_states').append('<div class="mh transbg block_'+idx+'"></div>');
								}
								else if(
									device['SwitchType']=='Dimmer'
								){
									$('.col1 .auto_dimmers').append('<div class="mh transbg block_'+idx+'"></div>');
								}
								else if(
									device['SwitchType']=='Media Player'
								){
									$('.col2 .auto_media').append('<div class="mh transbg block_'+idx+'"></div>');
								}
								else $('.col1 .auto_switches').append('<div class="mh transbg block_'+idx+'"></div>');
							}
							
							var buttonimg = '';
							if(device['Image']=='Fan') buttonimg = 'fan';
							if(device['Image']=='Heating') buttonimg = 'heating';
							
							$('div.block_'+idx).data('light',idx);
							$('div.block_'+idx).addClass('col-xs-'+width);
							
							
							var i=1;
							while (i <= 5) {
								if($('div.block_'+idx+'_'+i).length>0){
									$('div.block_'+idx+'_'+i).data('light',idx);
									$('div.block_'+idx+'_'+i).addClass('col-xs-'+width);
									$('div.block_'+idx+'_'+i).html('');
								}
								i++;
							}
							
							var addHTML=true;
							var html = '';

							if($('div.block_graph_'+idx).length>0){
								getGraphs(device,false);
							}
							
							if(typeof(device['SubType'])!=='undefined' && device['SubType'] in blocktypes['SubType']){
								html+= getStatusBlock(device,blocktypes['SubType'][device['SubType']]);
							}
							else if(typeof(device['HardwareType'])!=='undefined' && device['HardwareType'] in blocktypes['HardwareType']){
								html+= getStatusBlock(device,blocktypes['HardwareType'][device['HardwareType']]);
							}
							else if(typeof(device['HardwareName'])!=='undefined' && device['HardwareName'] in blocktypes['HardwareName']){
								html+= getStatusBlock(device,blocktypes['HardwareName'][device['HardwareName']]);
							}
							else if(typeof(device['Type'])!=='undefined' && device['Type'] in blocktypes['Type']){
								html+= getStatusBlock(device,blocktypes['Type'][device['Type']]);
							}
							else if(typeof(device['Name'])!=='undefined' && device['Name'] in blocktypes['Name']){
								html+= getStatusBlock(device,blocktypes['Name'][device['Name']]);
							}
							else if(device['Name']=='Thuis'){
							   $('.block_'+idx).attr('onclick','switchDevice(this)');
							   html+='<div class="col-xs-4 col-icon">';
								  if(device['Status']=='Off') html+='<img src="img/switch_off.png" class="off icon" />';
								  else html+='<img src="img/switch_on.png" class="on icon" />';
							   html+='</div>';
							   html+='<div class="col-xs-8 col-data">';
								  html+='<strong class="title">'+device['Name']+'</strong><br />';
								  if(device['Status']=='Off') html+='<span class="state">AFWEZIG</span>';
								  else html+='<span class="state">AANWEZIG</span>';
							   html+='</div>';
							}
							else if(device['HardwareType']=='Logitech Media Server'){
								html+=iconORimage(idx,'fa-music','','on icon','',2);
								html+='<div class="col-xs-10 col-data">';
								html+='<strong class="title">'+device['Name']+'</strong><br />';
								html+='<span class="h4">'+device['Data']+'</span>';
								html+='<div>';
									html+='<a href="javascript:controlLogitech('+device['idx']+',\'Rewind\');"><em class="fa fa-arrow-circle-left fa-small"></em></a> ';
									html+='<a href="javascript:controlLogitech('+device['idx']+',\'Stop\');"><em class="fa fa-stop-circle fa-small"></em></a> ';
									if(device['Status']=='Playing') html+='<a href="javascript:controlLogitech('+device['idx']+',\'Pause\');"><em class="fa fa-pause-circle fa-small"></em></a> ';
									else html+='<a href="javascript:controlLogitech('+device['idx']+',\'Play\');"><em class="fa fa-play-circle fa-small"></em></a> ';
									html+='<a href="javascript:controlLogitech('+device['idx']+',\'Forward\');"><em class="fa fa-arrow-circle-right fa-small"></em></a>';
								html+='</div>';
								html+='</div>';
								
								$('div.block_'+idx).addClass('with_controls');
							}
							else if(device['SwitchType'] == 'Media Player'){
								if(device['HardwareType']=='Kodi Media Server') html+=iconORimage(idx,'','kodi.png','on icon','',2);
								else html+=iconORimage(idx,'fa-film','','on icon','',2);
								html+='<div class="col-xs-10 col-data">';
								html+='<strong class="title">'+device['Name']+'</strong><br />';
								if(device['Data']==''){
									device['Data']=lang.mediaplayer_nothing_playing;
									if(_HIDE_MEDIAPLAYER_WHEN_OFF) $('div.block_'+idx).hide();
								}
								else {
									$('div.block_'+idx).show();
								}
								html+='<span class="h4">'+device['Data']+'</span>';
							}
							else if((device['HardwareType']=='Toon Thermostat' && device['SubType']!=='SetPoint' && device['SubType']!=='AC') || device['Type']=='P1 Smart Meter' || device['HardwareType']=='P1 Smart Meter USB'){
								if(device['Type']=='P1 Smart Meter' && device['SubType']=='Energy'){

									if($('div.block_'+idx).length>0){
										allblocks[idx] = true;
									}
									
									var title=lang.energy_usage;
									if(typeof(blocks[idx+'_1'])!=='undefined' && typeof(blocks[idx+'_1']['title'])!=='undefined') title=blocks[idx+'_1']['title'];
									html+= getStateBlock(device['idx']+'sub1','fa fa-plug',title,device['Usage'],device);
									if(!$('div.block_'+idx).hasClass('block_'+idx+'_1')) $('div.block_'+idx).addClass('block_'+idx+'_1');
									$('div.block_'+idx+'_1').html(html);
									addHTML=false;
									
									var title=lang.energy_usagetoday;
									if(typeof(blocks[idx+'_2'])!=='undefined' && typeof(blocks[idx+'_2']['title'])!=='undefined') title=blocks[idx+'_2']['title'];
									html = getStateBlock(device['idx']+'sub2','fa fa-plug',title,device['CounterToday'],device);
									if(typeof(allblocks[idx])!=='undefined' && $('div.block_'+idx+'_2').length==0) var duplicate = $('div.block_'+idx+'_1').last().clone().removeClass('block_'+idx+'_1').addClass('block_'+idx+'_2').insertAfter($('div.block_'+idx+'_1'));
									$('div.block_'+idx+'_2').html(html);
									addHTML=false;
								}
							}
							else if(device['Type']=='RFXMeter' && device['SubType']=='RFXMeter counter'){
								if($('div.block_'+idx).length>0){
									allblocks[idx] = true;
								}
								
								var rfxicon='fa fa-fire';
								if(device['Name']=='Water'){
									var rfxicon='fa fa-tint';
								}
								var title=device['Name'];
								if(typeof(blocks[idx+'_1'])!=='undefined' && typeof(blocks[idx+'_1']['title'])!=='undefined') title=blocks[idx+'_1']['title'];
								html+= getStateBlock(device['idx']+'a',rfxicon,title,device['CounterToday'],device);
								if(!$('div.block_'+idx).hasClass('block_'+idx+'_1')) $('div.block_'+idx).addClass('block_'+idx+'_1');
								$('div.block_'+idx+'_1').html(html);
								addHTML=false;
									
								var title=lang.energy_totals+' '+device['Name'];
								if(typeof(blocks[idx+'_2'])!=='undefined' && typeof(blocks[idx+'_2']['title'])!=='undefined') title=blocks[idx+'_2']['title'];
								html= getStateBlock(device['idx']+'b',rfxicon,title,device['Counter'],device);
								if(typeof(allblocks[idx])!=='undefined' && $('div.block_'+idx+'_2').length==0) var duplicate = $('div.block_'+idx+'_1').last().clone().removeClass('block_'+idx+'_1').addClass('block_'+idx+'_2').insertAfter($('div.block_'+idx+'_1'));
								$('div.block_'+idx+'_2').html(html);
								addHTML=false;
							}

							else if(device['Type']=='General' && device['SubType']=='kWh'){
								if($('div.block_'+idx).length>0){
									allblocks[idx] = true;
								}
								
								var title=device['Name']+' '+lang.energy_now;
								if(typeof(blocks[idx+'_1'])!=='undefined' && typeof(blocks[idx+'_1']['title'])!=='undefined') title=blocks[idx+'_1']['title'];
								html+= getStateBlock(device['idx']+'a','fa fa-plug',title,number_format(device['Usage'],2,',','.')+' W',device);
								if(!$('div.block_'+idx).hasClass('block_'+idx+'_1')) $('div.block_'+idx).addClass('block_'+idx+'_1');
								$('div.block_'+idx+'_1').html(html);
								addHTML=false;
									
								var title=device['Name']+' '+lang.energy_today;
								if(typeof(blocks[idx+'_2'])!=='undefined' && typeof(blocks[idx+'_2']['title'])!=='undefined') title=blocks[idx+'_2']['title'];
								html= getStateBlock(device['idx']+'b','fa fa-plug',title,number_format(device['CounterToday'],2,',','.')+' kWh',device);
								if(typeof(allblocks[idx])!=='undefined' && $('div.block_'+idx+'_2').length==0) var duplicate = $('div.block_'+idx+'_1').last().clone().removeClass('block_'+idx+'_1').addClass('block_'+idx+'_2').insertAfter($('div.block_'+idx+'_1'));
								$('div.block_'+idx+'_2').html(html);
								addHTML=false;
								
								var title=device['Name']+' '+lang.energy_total;
								if(typeof(blocks[idx+'_3'])!=='undefined' && typeof(blocks[idx+'_3']['title'])!=='undefined') title=blocks[idx+'_3']['title'];
								html= getStateBlock(device['idx']+'c','fa fa-plug',title,number_format(device['Data'],2,',','.')+' kWh',device);
								if(typeof(allblocks[idx])!=='undefined' && $('div.block_'+idx+'_3').length==0) var duplicate = $('div.block_'+idx+'_2').last().clone().removeClass('block_'+idx+'_2').addClass('block_'+idx+'_3').insertAfter($('div.block_'+idx+'_2'));
								$('div.block_'+idx+'_3').html(html);
								addHTML=false;
								
							}
							else if(
								device['Type']=='Temp + Humidity + Baro' || 
								device['Type']=='Temp + Humidity' || 
								device['Type']=='Humidity'
							){
								
								if($('div.block_'+idx).length>0){
									allblocks[idx] = true;
								}
								
								var title=device['Name'];
								if(typeof(blocks[idx+'_1'])!=='undefined' && typeof(blocks[idx+'_1']['title'])!=='undefined') title=blocks[idx+'_1']['title'];
								html+= getStateBlock(device['idx']+'a','fa fa-thermometer-half',title,number_format(device['Temp'],1,',','.')+_TEMP_SYMBOL,device);
								if(!$('div.block_'+idx).hasClass('block_'+idx+'_1')) $('div.block_'+idx).addClass('block_'+idx+'_1');
								$('div.block_'+idx+'_1').html(html);
								addHTML=false;
									
								if(typeof(device['Humidity'])!=='undefined'){
									var title=device['Name'];
									if(typeof(blocks[idx+'_2'])!=='undefined' && typeof(blocks[idx+'_2']['title'])!=='undefined') title=blocks[idx+'_2']['title'];
									html= getStateBlock(device['idx']+'b','wi wi-humidity',title,number_format(device['Humidity'],2,',','.')+'%',device);
									if(typeof(allblocks[idx])!=='undefined' && $('div.block_'+idx+'_2').length==0) var duplicate = $('div.block_'+idx+'_1').last().clone().removeClass('block_'+idx+'_1').addClass('block_'+idx+'_2').insertAfter($('div.block_'+idx+'_1'));
									$('div.block_'+idx+'_2').html(html);
									addHTML=false;
								}
								
								if(typeof(device['Barometer'])!=='undefined'){
									var title=device['Name'];
									if(typeof(blocks[idx+'_3'])!=='undefined' && typeof(blocks[idx+'_3']['title'])!=='undefined') title=blocks[idx+'_3']['title'];
									html= getStateBlock(device['idx']+'c','wi wi-barometer',title,device['Barometer']+' hPa',device);
									if(typeof(allblocks[idx])!=='undefined' && $('div.block_'+idx+'_3').length==0) var duplicate = $('div.block_'+idx+'_2').last().clone().removeClass('block_'+idx+'_2').addClass('block_'+idx+'_3').insertAfter($('div.block_'+idx+'_2'));
									$('div.block_'+idx+'_3').html(html);
									addHTML=false;
								}
								
							}
							else if(device['SwitchType']=='Dimmer'){
								if(buttonimg==''){
									if(device['Status']=='Off') html+=iconORimage(idx,'fa-lightbulb-o','','off icon iconslider','',2,'data-light="'+device['idx']+'" onclick="switchDevice(this);"');
									else html+=iconORimage(idx,'fa-lightbulb-o','','on icon iconslider','',2,'data-light="'+device['idx']+'" onclick="switchDevice(this);"');
								}
								else {
									if(device['Status']=='Off') html+=iconORimage(idx,'',buttonimg+'.png','off icon iconslider','',2,'data-light="'+device['idx']+'" onclick="switchDevice(this);"');
									else html+=iconORimage(idx,'',buttonimg+'.png','on icon iconslider','',2,'data-light="'+device['idx']+'" onclick="switchDevice(this);"');
								}
								html+='<div class="col-xs-10 col-data">';
									html+='<strong class="title">'+device['Name']+': '+device['Level']+'%'+'</strong><br />';
									html+='<div class="slider slider'+device['idx']+'" data-light="'+device['idx']+'"></div>';
								html+='</div>';

								$('div.block_'+idx).html(html);
								addHTML=false;
								if(parseFloat(device['MaxDimLevel'])==100){
									$( ".slider"+device['idx'] ).slider({
										value:device['Level'],
										step: 1,
										min:1,
										max:100,
										slide: function( event, ui ) {
											sliding = true;
											slideDevice($(this).data('light'),ui.value);
										},
										stop: function( event, ui ) {
											sliding = false;
										}
									});
								}
								else /*if(parseFloat(device['MaxDimLevel'])==15)*/{
									$( ".slider"+device['idx'] ).slider({
										value:Math.ceil((device['Level']/100)*15),
										step: 1,
										min:2,
										max:15,
										slide: function( event, ui ) {
											sliding = true;
											slideDevice($(this).data('light'),ui.value);
										},
										stop: function( event, ui ) {
											sliding = false;
										}
									});
								}
							}
							else if(device['Type']=='Group' || device['Type']=='Scene'){
								if(device['Type']=='Group') $('.block_'+idx).attr('onclick','switchDevice(this)');
								if(device['Type']=='Scene') $('.block_'+idx).attr('onclick','switchGroup(this)');
								
								if(buttonimg==''){
									if(device['Status']=='Off') html+=iconORimage(idx,'fa-lightbulb-o','','off icon');
									else html+=iconORimage(idx,'fa-lightbulb-o','','on icon');
								}
								else {
									if(device['Status']=='Off') html+=iconORimage(idx,'',buttonimg+'.png','off icon');
									else html+=iconORimage(idx,'',buttonimg+'.png','on icon');	
								}
								
								html+=getBlockData(device,idx,lang.state_on,lang.state_off);
							}
							else if(typeof(device['LevelActions'])!=='undefined' && device['LevelActions']!==""){
								var names = device['LevelNames'].split('|');
	
								if(buttonimg==''){
									html+=iconORimage(idx,'fa-lightbulb-o','','on icon');
								}
								else {
									html+=iconORimage(idx,'',buttonimg+'.png','on icon');	
								}
										
								html+='<div class="col-xs-8 col-data">';
									html+='<strong class="title">'+device['Name']+'</strong><br />';
									html+='<select onchange="slideDevice('+device['idx']+',this.value);">';
									html+='<option value="">'+lang.select+'</option>';
									for(a in names){
										var s='';
										if((a*10)==parseFloat(device['Level'])) s = 'selected';
										html+='<option value="'+(a*10)+'" '+s+'>'+names[a]+'</option>';
									}
									html+='</select>';
								html+='</div>';
							}
							else if((device['Type']=='Thermostat' || device['HardwareType']=='Toon Thermostat') && device['SubType']=='SetPoint'){

								html+=iconORimage(idx+'_1','','heating.png','on icon','style="max-height:35px;"');
								html+='<div class="col-xs-8 col-data">';
									if(typeof(blocks[idx])!=='undefined' && typeof(blocks[idx]['switch'])!=='undefined' && blocks[idx]['switch']==true){
										html+='<strong class="title">'+device['Name']+'</strong><br />';
										html+='<span class="state">'+device['Data']+_TEMP_SYMBOL+'</span>';
									}
									else {
										html+='<strong class="title">'+device['Data']+_TEMP_SYMBOL+'</strong><br />';
										html+='<span class="state">'+device['Name']+'</span>';
									}
								html+='</div>';
								
								$('div.block_'+idx+'_1').html(html);
								addHTML=false;

								html =iconORimage(idx+'_2','','heating.png','on icon');
								html+='<div class="col-xs-8 col-data">';
									html+='<strong class="title">'+device['Data']+_TEMP_SYMBOL+'</strong><br />';
									html+='<select data-light="'+device['idx']+'" onchange="switchThermostat(this.value,this);">';
									html+='<option value="">'+device['Name']+'</option>';

									var i=12;
									var max=25;
									while (i <= max) {
										var s1='';
										html+='<option value="'+i+'" '+s1+'>'+i+' '+_TEMP_SYMBOL+'</option>';

										if(i<max){
											var s2='';
											html+='<option value="'+i+'.5" '+s2+'>'+i+'.5 '+_TEMP_SYMBOL+'</option>';
										}
										i++;
									}

									html+='</select>';
								html+='</div>';
								
								$('div.block_'+idx+'_2').html(html);
								$('div.block_'+idx).html(html);
								addHTML=false;
							}
							else if(device['SwitchType']=='Door Contact' || device['SwitchType']=='Door Lock'){
								html+='<div class="col-xs-4 col-icon">';
									if(device['Status']=='Closed') html+='<img src="img/door_closed.png" class="off icon" />';
									else html+='<img src="img/door_open.png" class="on icon" />';
								html+='</div>';
								html+=getBlockData(device,idx,lang.state_open,lang.state_closed);
							}
							else if(device['SwitchType']=='Contact'){
								html+='<div class="col-xs-4 col-icon">';
									if(device['Status']=='Closed') html+='<img src="img/door_closed.png" class="off icon" />';
									else html+='<img src="img/door_open.png" class="on icon" />';
								html+='</div>';
								html+=getBlockData(device,idx,lang.state_open,lang.state_closed);
							}
							else if(device['SubType']=='Custom Sensor'){
								
								if(device['Image']=='Water') html+=iconORimage(idx,'fa-tint','','on icon');
								else if(device['Image']=='Heating') html+=iconORimage(idx,'fa-cutlery','','on icon');
								else html+=iconORimage(idx,'fa-question','','on icon');
								
								html+='<div class="col-xs-8 col-data">';
									if(typeof(blocks[idx])!=='undefined' && typeof(blocks[idx]['switch'])!=='undefined' && blocks[idx]['switch']==true){
										html+='<strong class="title">'+device['Data']+'</strong><br />';
										html+='<span class="state">'+device['Name']+'</span>';
									}
									else {
										html+='<strong class="title">'+device['Name']+'</strong><br />';
										html+='<span class="state">'+device['Data']+'</span>';
									}
								html+='</div>';
							}
							else if(device['SwitchType']=='Venetian Blinds EU' || device['SwitchType']=='Blinds' || 
								   device['SwitchType']=='Venetian Blinds EU Percentage' || device['SwitchType']=='Blinds Percentage' || 
								   device['SwitchType']=='Venetian Blinds EU Inverted' || device['SwitchType']=='Blinds Inverted'){
								html+='<div class="col-xs-4 col-icon">';
								   if(device['Status']=='Closed') html+='<img src="img/blinds_closed.png" class="off icon" />';
								   else html+='<img src="img/blinds_open.png" class="on icon" />';
								html+='</div>';
								html+='<div class="col-xs-8 col-data">';
								   html+='<strong class="title">'+device['Name']+'</strong><br />';
								   html+='<div class="blinds">';
										if(device['SwitchType']=='Venetian Blinds EU Inverted' || device['SwitchType']=='Blinds Inverted'){
											html+='<a href="javascript:switchBlinds('+device['idx']+',\'On\');"><em class="fa fa-arrow-up fa-small"></em></a> ';
											html+='<a href="javascript:switchBlinds('+device['idx']+',\'Stop\');"><em class="fa fa-stop-circle fa-small"></em></a> ';
											html+='<a href="javascript:switchBlinds('+device['idx']+',\'Off\');"><em class="fa fa-arrow-down fa-small"></em></a>';
										}
										else{
											html+='<a href="javascript:switchBlinds('+device['idx']+',\'Off\');"><em class="fa fa-arrow-up fa-small"></em></a> ';
											html+='<a href="javascript:switchBlinds('+device['idx']+',\'Stop\');"><em class="fa fa-stop-circle fa-small"></em></a> ';
											html+='<a href="javascript:switchBlinds('+device['idx']+',\'On\');"><em class="fa fa-arrow-down fa-small"></em></a>';
										}
								   html+='</div>';
								html+='</div>';
							}
							else if(device['SwitchType']=='Motion Sensor'){
								html+='<div class="col-xs-4 col-icon">';
								
									if(device['Status']=='Off' || device['Status']=='Normal') html+='<img src="img/motion_off.png" class="off icon" style="max-height:35px;" />';
									else html+='<img src="img/motion_on.png" class="on icon" style="max-height:35px;" />';	
								
								html+='</div>';
								html+=getBlockData(device,idx,lang.state_movement,lang.state_nomovement);
							}
							else if(device['SwitchType']=='Smoke Detector'){
								if(device['Status']=='Off' || device['Status']=='Normal') html+=iconORimage(idx,'','heating.png','off icon','style="max-height:35px;"');
								else html+=iconORimage(idx,'','heating.png','on icon','style="max-height:35px;border: 5px solid #F05F40;"');	
								html+=getBlockData(device,idx,lang.state_smoke,lang.state_nosmoke);
							}
							else if(device['HardwareName']=='Dummy') { 
								$('.block_'+idx).attr('onclick','switchDevice(this)');
								
								if(device['Status']=='Off') html+=iconORimage(idx,'fa-toggle-off','','off icon');
								else html+=iconORimage(idx,'fa-toggle-on','','on icon');
								
								html+=getBlockData(device,idx,lang.state_on,lang.state_off);
							}
							else if(device['Image']=='Alarm') { 
								if(device['Status']=='Off') html+=iconORimage(idx,'fa-warning','','off icon');
								else html+=iconORimage(idx,'fa-warning','','on icon','style="color:#F05F40;"');
								
								html+=getBlockData(device,idx,lang.state_on,lang.state_off);
							}
							else {
								$('.block_'+idx).attr('onclick','switchDevice(this)');
								if(buttonimg==''){
									if(device['Status']=='Off') html+=iconORimage(idx,'fa-lightbulb-o','','off icon');
									else html+=iconORimage(idx,'fa-lightbulb-o','','on icon');
								}
								else {
									if(device['Status']=='Off') html+=iconORimage(idx,'',buttonimg+'.png','off icon');
									else html+=iconORimage(idx,'',buttonimg+'.png','on icon');
								}
								html+=getBlockData(device,idx,lang.state_on,lang.state_off);
							}
							
							if(typeof($('.block_'+idx).attr('onclick'))!=='undefined'){
								$('div.block_'+idx).addClass('hover');	
							}
							if(addHTML){
								$('div.block_'+idx).html(html);
							}
						}
					}
				}
				
				if(typeof(_DEBUG)=='undefined' || _DEBUG===false) setTimeout(function(){ getDevices(); },5000);
			}
		});
	}
	else {
		if(typeof(_DEBUG)=='undefined' || _DEBUG===false) setTimeout(function(){ getDevices(); },5000);
	}
}

blocktypes = {}
blocktypes.SubType = {}
blocktypes.SubType['Visibility'] = { icon: 'fa fa-eye', title: '<Name>', value: '<Data>' }
blocktypes.SubType['Electric'] = { icon: 'fa fa-plug', title: '<Name>', value: '<Data>' }
blocktypes.SubType['Lux'] = { icon: 'fa fa-sun-o', title: '<Name>', value: '<Data>' }
blocktypes.SubType['Barometer'] = { icon: 'wi wi-barometer', title: '<Name>', value: '<Data>' }
blocktypes.SubType['Gas'] = { icon: 'fa fa-fire', title: lang.gas_usagetoday, value: '<CounterToday>' }
blocktypes.SubType['Sound Level'] = { icon: 'fa fa-volume-up', title: '<Name>', value: '<Data>' }
blocktypes.SubType['Percentage'] = { icon: 'fa fa-desktop', title: '<Name>', value: '<Data>' }
blocktypes.SubType['Distance'] = { icon: 'fa fa-eye', title: '<Name>', value: '<Data>' }
blocktypes.SubType['Alert'] = { icon: 'fa fa-warning', title: '<Data>', value: '<Name>' }

blocktypes.Type = {}
blocktypes.Type['Rain'] = { icon: 'fa fa-tint', title: '<Name>', value: '<Rain>mm' }
blocktypes.Type['Wind'] = { icon: 'wi wi-wind-direction', title: lang.wind, value: '<Speed> m/s, <Direction> <DirectionStr>' }
blocktypes.Type['Temp'] = { icon: 'fa fa-thermometer-half', title: '<Name>', value: '<Temp>'+_TEMP_SYMBOL }
blocktypes.Type['Air Quality'] = { image: 'air.png', title: '<Name>', value: '<Data>' }

blocktypes.HardwareType = {}
blocktypes.HardwareType['Motherboard sensors'] = { icon: 'fa fa-desktop', title: '<Name>', value: '<Data>' }
blocktypes.HardwareType['PVOutput (Input)'] = { icon: 'fa fa-sun-o', title: '<Name>', value: '<CounterToday>' }

blocktypes.HardwareName = {}
blocktypes.HardwareName['Rain expected'] = { icon: 'fa fa-tint', title: '<Data>', value: '<Name>' }

blocktypes.Name = {}
blocktypes.Name['Rain Expected'] = { icon: 'fa fa-tint', title: '<Data>', value: '<Name>' }
blocktypes.Name['Rain expected'] = { icon: 'fa fa-tint', title: '<Data>', value: '<Name>' }
blocktypes.Name['Regen mm/uur'] = { icon: 'fa fa-tint', title: '<Data>', value: '<Name>' }
blocktypes.Name['Regen verwacht'] = { icon: 'fa fa-tint', title: '<Data>', value: '<Name>' }
blocktypes.Name['Regen Verwacht'] = { icon: 'fa fa-tint', title: '<Data>', value: '<Name>' }

blocktypes.Name['Ping'] = { icon: 'fa fa-arrows-v', title: '<Name>', value: '<Data>' }
blocktypes.Name['Upload'] = { icon: 'fa fa-long-arrow-up', title: '<Name>', value: '<Data>' }
blocktypes.Name['Download'] = { icon: 'fa fa-long-arrow-down', title: '<Name>', value: '<Data>' }









function getStateBlock(id,icon,title,value,device){
	
	if(device['TypeImg']=='counter'){
		getButtonGraphs(device);
		$('.block_'+device['idx']).addClass('hover');
		$('.block_'+device['idx']).attr('data-toggle','modal');
		$('.block_'+device['idx']).attr('data-target','#opengraph'+device['idx']);
	}
	
	var stateBlock ='<div class="col-xs-4 col-icon">';
		stateBlock+='<em class="'+icon+'"></em>';
	stateBlock+='</div>';
	stateBlock+='<div class="col-xs-8 col-data">';
		if(typeof(blocks[device['idx']])!=='undefined' && typeof(blocks[device['idx']]['switch'])!=='undefined' && blocks[device['idx']]['switch']==true){
			stateBlock+='<strong class="title">'+title+'</strong><br />';
			stateBlock+='<span>'+value+'</span>';
		}
		else {
			stateBlock+='<strong class="title">'+value+'</strong><br />';
			stateBlock+='<span>'+title+'</span>';

		}
	stateBlock+='</div>';
	return stateBlock;
}


function getStatusBlock(device,block){
	
	var value = block.value;
	var title = block.title;
	
	for(d in device) {
		value = value.replace('<'+d+'>',device[d]);
		title = title.replace('<'+d+'>',device[d]);
	}
						
	if(device['TypeImg']=='counter' || device['Type']=='Temp' || device['Type']=='Wind' || device['Type']=='Rain' || device['Type']== 'Temp + Humidity' || device['Type']== 'Temp + Humidity + Baro'){
		getButtonGraphs(device);
		$('.block_'+device['idx']).addClass('hover');
		$('.block_'+device['idx']).attr('data-toggle','modal');
		$('.block_'+device['idx']).attr('data-target','#opengraph'+device['idx']);
	}
	
	var attr='';
	if(typeof(device['Direction'])!=='undefined' && typeof(device['DirectionStr'])!=='undefined'){
		attr+=' style="-webkit-transform: rotate('+device['Direction']+'deg);-moz-transform: rotate('+device['Direction']+'deg);-ms-transform: rotate('+device['Direction']+'deg);-o-transform: rotate('+device['Direction']+'deg); transform: rotate('+device['Direction']+'deg);"';
	}
	var stateBlock ='<div class="col-xs-4 col-icon">';
		if(typeof(blocks[device['idx']])!=='undefined' && typeof(blocks[device['idx']]['icon'])!=='undefined'){
			stateBlock+='<em class="fa '+blocks[device['idx']]['icon']+'"'+attr+'></em>';
		}
		else if(typeof(blocks[device['idx']])!=='undefined' && typeof(blocks[device['idx']]['image'])!=='undefined'){
			stateBlock+='<img src="img/'+blocks[device['idx']]['image']+'"'+attr+' class="icon" />';
		}
		else {
			if(typeof(block.image)!=='undefined') stateBlock+='<img src="img/'+block.image+'"'+attr+' class="icon" />';
			else stateBlock+='<em class="'+block.icon+'"'+attr+'></em>';
		}
	stateBlock+='</div>';
	stateBlock+='<div class="col-xs-8 col-data">';
		if(typeof(blocks[device['idx']])!=='undefined' && typeof(blocks[device['idx']]['switch'])!=='undefined' && blocks[device['idx']]['switch']==true){
			stateBlock+='<strong class="title">'+title+'</strong><br />';
			stateBlock+='<span>'+value+'</span>';
		}
		else {
			stateBlock+='<strong class="title">'+value+'</strong><br />';
			stateBlock+='<span>'+title+'</span>';
		}
	stateBlock+='</div>';
	return stateBlock;
}


function iconORimage(idx,defaulticon,defaultimage,classnames,attr,colwidth,attrcol){
	if(typeof(colwidth)=='undefined') colwidth=4;
	if(typeof(attrcol)=='undefined') attrcol='';
	var icon = '<div class="col-xs-'+colwidth+' col-icon" '+attrcol+'>';
	if(typeof(blocks[idx])!=='undefined' && typeof(blocks[idx]['icon'])!=='undefined'){
		icon+='<em class="fa '+blocks[idx]['icon']+' '+classnames+'" '+attr+'></em>';
	}
	else if(typeof(blocks[idx])!=='undefined' && typeof(blocks[idx]['image'])!=='undefined'){
		icon+='<img src="img/'+blocks[idx]['image']+'" class="'+classnames+'" '+attr+' />';
	}
	else if(defaulticon!=='') icon+='<em class="fa '+defaulticon+' '+classnames+'" '+attr+'></em>';
	else if(defaultimage!=='') icon+='<img src="img/'+defaultimage+'" class="'+classnames+'" '+attr+' />';
	
	icon+='</div>';
	return icon;
}

function getBlockData(device,idx,ontxt,offtxt){
	var data='<div class="col-xs-8 col-data">';
	if(typeof(blocks[idx])!=='undefined' && typeof(blocks[idx]['switch'])!=='undefined' && blocks[idx]['switch']==true){
		if(device['Status']=='Off' || device['Status']=='Closed') data+='<strong class="title">'+offtxt+'</strong><br />';
		else data+='<strong class="title">'+ontxt+'</strong><br />';
		data+='<span class="state">'+device['Name']+'</span><br />';
	}
	else {
		data+='<strong class="title">'+device['Name']+'</strong><br />';
		if(device['Status']=='Off' || device['Status']=='Closed') data+='<span class="state">'+offtxt+'</span>';
		else data+='<span class="state">'+ontxt+'</span>';
	}
	data+='</div>';
	return data;
}

function getNews(divToFill,newsfeed){

	if(typeof(_NEWS_RSSFEED)!=='undefined'){
		$.ajax('https://crossorigin.me/'+newsfeed, {
			accepts:{
				xml:"application/rss+xml"
			},
			dataType:"xml",
			success:function(data) {

				var width=12;
				if(typeof(blocks[divToFill])!=='undefined' && typeof(blocks[divToFill]['width'])!=='undefined') width=blocks[divToFill]['width'];

				var html = '<div class="col-xs-'+width+' hover transbg"><div class="col-xs-2 col-icon"><em class="fa fa-newspaper-o"></em></div><div class="col-xs-10">';
				html+='<div id="rss-styled_'+divToFill+'"><ul id="newsTicker">';

				$(data).find("item").each(function () { // or "item" or whatever suits your feed
					var el = $(this);
					html+='<li><strong>'+el.find("title").text()+'</strong><br />'+el.find("description").text()+'</li>';
				});

				html+='</div></div></div>';
				$("div."+divToFill).replaceWith('<div class="'+divToFill+'">'+html+'</div>');

				newsWrapper = $('#rss-styled_'+divToFill).easyTicker({
					direction: 'up',
					easing: 'lineair',
					speed: 'slow',
					interval: _SCROLL_NEWS_AFTER,
					visible: 1,
					mousePause: 0
				}).data('easyTicker');
			}
			
		});	

		setTimeout(function(){getNews(divToFill);}, (60000*5));
	}
}
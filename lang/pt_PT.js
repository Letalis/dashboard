var lang = new Object();
lang['state_on'] = 'LIGADO';
lang['state_off'] = 'DESLIGADO';
lang['state_open'] = 'ABERTO';
lang['state_closed'] = 'FECHADO';
lang['state_movement'] = 'MOVIMENTO';
lang['state_nomovement'] = 'Sem movimento';
lang['state_switch'] = 'Interruptor';
lang['state_nosmoke'] = 'SEM FUMO';
lang['state_smoke'] = 'FUMO DETETADO!';

lang['monday'] = 'Segunda';
lang['tuesday'] = 'Ter&ccedila';
lang['wednesday'] = 'Quarta';
lang['thursday'] = 'Quinta';
lang['friday'] = 'Sexta';
lang['saturday'] = 'S&aacute;bado';
lang['sunday'] = 'Domingo';

lang['graph_last_hours'] = '&Uacute;ltimas horas';
lang['graph_today'] = 'Hoje';
lang['graph_last_month'] = '&Uacute;ltimo mes';

lang['mediaplayer_nothing_playing'] = 'Agora nada está a tocar';
lang['energy_usage'] = 'Energia';
lang['energy_usagetoday'] = 'Energia hoje';
lang['energy_totals'] = 'Total';
lang['energy_total'] = 'Total';
lang['energy_now'] = 'Agora';
lang['energy_today'] = 'Hoje';
lang['gas_usagetoday'] = 'G&aacute;s hoje';
lang['temp_toon'] = 'Sala de estar';
lang['wind'] = 'Vento';
lang['select'] = 'Selecionar';
lang['notifications_ns'] = 'notifica&ccedil&otilde;es adicionada por NS';
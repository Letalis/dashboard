var lang = new Object();
lang['state_on'] = 'BE';
lang['state_off'] = 'KI';
lang['state_open'] = 'NYITVA';
lang['state_closed'] = 'ZÁRVA';
lang['state_movement'] = 'MOZGÁS';
lang['state_nomovement'] = 'NINCS MOZGÁS';
lang['state_switch'] = 'Kapcsoló';
lang['state_nosmoke'] = 'Nincs füst';
lang['state_smoke'] = 'FÜST ÉRZÉKELVE!';

lang['monday'] = 'Hétfő';
lang['tuesday'] = 'Kedd';
lang['wednesday'] = 'Szerda';
lang['thursday'] = 'Csütörtök';
lang['friday'] = 'Péntek';
lang['saturday'] = 'Szombat';
lang['sunday'] = 'Vasárnap';

lang['graph_last_hours'] = 'utolsó órák';
lang['graph_today'] = 'ma';
lang['graph_last_month'] = 'utolsó hónap';

lang['mediaplayer_nothing_playing'] = 'Semmi sem játszódik most';
lang['energy_usage'] = 'Energia';
lang['energy_usagetoday'] = 'Energiafogyasztás';
lang['energy_totals'] = 'Összesen';
lang['energy_total'] = 'összesen';
lang['energy_now'] = 'most';
lang['energy_today'] = 'ma';
lang['gas_usagetoday'] = 'Mai gáz';
lang['temp_toon'] = 'Nappali';
lang['wind'] = 'Szél';
lang['select'] = 'Kiválaszt';
lang['notifications_ns'] = 'notifications added by NS';

var lang = new Object();
lang['state_on'] = 'AAN';
lang['state_off'] = 'UIT';
lang['state_open'] = 'OPEN';
lang['state_closed'] = 'DICHT';
lang['state_movement'] = 'BEWEGING';
lang['state_nomovement'] = 'Geen beweging';
lang['state_switch'] = 'Schakelen';
lang['state_nosmoke'] = 'Geen rook';
lang['state_smoke'] = 'ROOK GEDETECTEERD!';

lang['monday'] = 'Maandag';
lang['tuesday'] = 'Dinsdag';
lang['wednesday'] = 'Woensdag';
lang['thursday'] = 'Donderdag';
lang['friday'] = 'Vrijdag';
lang['saturday'] = 'Zaterdag';
lang['sunday'] = 'Zondag';

lang['graph_last_hours'] = 'laatste uren';
lang['graph_today'] = 'vandaag';
lang['graph_last_month'] = 'afgelopen maand';

lang['mediaplayer_nothing_playing'] = 'Er wordt momenteel niets afgespeeld';
lang['energy_usage'] = 'Energieverbruik';
lang['energy_usagetoday'] = 'Energie vandaag';
lang['energy_totals'] = 'Totaal';
lang['energy_total'] = 'totaal';
lang['energy_now'] = 'nu';
lang['energy_today'] = 'vandaag';
lang['gas_usagetoday'] = 'Gas vandaag';
lang['temp_toon'] = 'Woonkamer';
lang['wind'] = 'Kracht';
lang['select'] = 'Selecteer';
lang['notifications_ns'] = 'meldingen geplaatst door de NS';
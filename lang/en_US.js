
var lang = new Object();
lang['state_on'] = 'ON';
lang['state_off'] = 'OFF';
lang['state_open'] = 'OPEN';
lang['state_closed'] = 'CLOSED';
lang['state_movement'] = 'MOVEMENT';
lang['state_nomovement'] = 'No movement';
lang['state_switch'] = 'Switch';
lang['state_nosmoke'] = 'No smoke';
lang['state_smoke'] = 'SMOKE DETECTED!';

lang['monday'] = 'Monday';
lang['tuesday'] = 'Tuesday';
lang['wednesday'] = 'Wednesday';
lang['thursday'] = 'Thursday';
lang['friday'] = 'Friday';
lang['saturday'] = 'Saturday';
lang['sunday'] = 'Sunday';

lang['graph_last_hours'] = 'last hours';
lang['graph_today'] = 'today';
lang['graph_last_month'] = 'last month';

lang['mediaplayer_nothing_playing'] = 'Nothing is playing right now';
lang['energy_usage'] = 'Energy';
lang['energy_usagetoday'] = 'Energy today';
lang['energy_totals'] = 'Total';
lang['energy_total'] = 'total';
lang['energy_now'] = 'now';
lang['energy_today'] = 'today';
lang['gas_usagetoday'] = 'Gas today';
lang['temp_toon'] = 'Living room';
lang['wind'] = 'Wind';
lang['select'] = 'Select';
lang['notifications_ns'] = 'notifications added by NS';

var lang = new Object();
lang['state_on'] = 'Allumé';
lang['state_off'] = 'Eteint';
lang['state_open'] = 'Ouvert';
lang['state_closed'] = 'Fermé';
lang['state_movement'] = 'MOUVEMENT';
lang['state_nomovement'] = 'Aucun mouvement';
lang['state_switch'] = 'Commuter';
lang['state_nosmoke'] = 'Pas de fumée';
lang['state_smoke'] = 'FUMÉE DETECTEE!';

lang['monday'] = 'Lundi';
lang['tuesday'] = 'Mardi';
lang['wednesday'] = 'Mercredi';
lang['thursday'] = 'Jeudi';
lang['friday'] = 'Vendredi';
lang['saturday'] = 'Samedi';
lang['sunday'] = 'Dimanche';

lang['graph_last_hours'] = 'Dernières heures';
lang['graph_today'] = 'Aujourd hui';
lang['graph_last_month'] = 'Mois dernier';

lang['mediaplayer_nothing_playing'] = 'Il n\'y a pas de jeu';
lang['energy_usage'] = 'Énergie';
lang['energy_usagetoday'] = 'Énergie Aujourd hui';
lang['energy_totals'] = 'Total';
lang['energy_total'] = 'total';
lang['energy_now'] = 'maintenant';
lang['energy_today'] = 'Énergie Aujourd hui';
lang['gas_usagetoday'] = 'Gaz aujourd hui';
lang['temp_toon'] = 'Température';
lang['wind'] = 'Force';
lang['select'] = 'Sélectionnez';
lang['notifications_ns'] = 'messages postés par le NS';